# README #

Welcome!

### What is this repository for? ###

This sample project shows an enhancement of displaying objects by the centre of user's location.

### How do I get set up? ###

Checkout the code, import into Android Studio building by gradle.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact