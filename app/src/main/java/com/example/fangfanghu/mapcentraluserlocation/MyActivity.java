package com.example.fangfanghu.mapcentraluserlocation;

import android.app.Dialog;
import android.app.Fragment;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;


public class MyActivity extends FragmentActivity implements LocationListener {

    private GoogleMap googleMap;
    private LatLngBounds locations;
    private LatLng myStand;
    private LatLng neighbour1;
    private LatLng northeast;
    private LatLng southwest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        // Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        if(status!= ConnectionResult.SUCCESS){ // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        }else { // Google Play Services are available

            // Getting reference to the SupportMapFragment of activity_main.xml
            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

            // Getting GoogleMap object from the fragment
            googleMap = fm.getMap();

            // Enabling MyLocation Layer of Google Map
            googleMap.setMyLocationEnabled(true);


            googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(locations, 100));
                    googleMap.addMarker(new MarkerOptions()
                            .position(myStand)
                            .title("Me"));
                    googleMap.addMarker(new MarkerOptions()
                            .position(neighbour1)
                            .title("Neighbour"));

                    northeast = googleMap.getProjection().getVisibleRegion().latLngBounds.northeast;
                    southwest = googleMap.getProjection().getVisibleRegion().latLngBounds.southwest;

                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(myStand));

                }
            });

            googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    if(googleMap!=null && northeast !=null && southwest!=null){
                        while(!(googleMap.getProjection().getVisibleRegion().latLngBounds.contains(northeast)&&googleMap.getProjection().getVisibleRegion().latLngBounds.contains(southwest))){
                            googleMap.moveCamera(CameraUpdateFactory.zoomOut());
                        }
                    }
                }
            });

            // Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();

            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);

            // Getting Current Location
            Location location = locationManager.getLastKnownLocation(provider);

            if(location!=null){
                onLocationChanged(location);
            }

            long l = 20000;
            float f = 0;
//            locationManager.requestLocationUpdates(provider, l, f, this);

        }

    }

    @Override
    public void onLocationChanged(Location location) {
        if(location!=null){
            myStand = new LatLng(location.getLatitude(), location.getLongitude());
            neighbour1 = new LatLng(location.getLatitude()-0.001, location.getLongitude()-0.001);

            locations = LatLngBounds.builder().include(myStand).include(neighbour1).build();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_my, container, false);
            return rootView;
        }
    }
}
